#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define NORM_PW 2


typedef struct point {
    double x, y;
    int clusterNumber;
} point;

typedef struct cluster {
    point centroid;
} cluster;

double distance(point p1, point p2) {
    point p;
    p.x = pow(p1.x - p2.x, NORM_PW);
    p.y = pow(p1.y - p2.y, NORM_PW);
    return sqrt(p.x + p.y);
}

point sum(point p1, point p2) {
    point p;
    p.y = p1.y + p2.y;
    p.x = p1.x + p2.x;
    return p;
}

point divide(point p, int n) {
    point po;
    po.x = p.x / n;
    po.y = p.y / n;

    return po;
}

void printClusters(int nCluster, int nPoints, point *points, cluster *clusters) {
    //for each cluster
    for (int currentCluster = 0; currentCluster < nCluster; ++currentCluster) {
        int n_points = 0;
        printf("Cluster:%d\n", currentCluster + 1);
        point currentCentroid = clusters[currentCluster].centroid;
        printf("Centroid:(%lf, %lf)\n", currentCentroid.x, currentCentroid.y);
        printf("points:");
        //for each point
        for (int currentPoint = 0; currentPoint < nPoints; ++currentPoint) {
            //if the point belongs to the cluster
            if (points[currentPoint].clusterNumber == currentCluster) {
                printf("(%lf,%lf) ", points[currentPoint].x, points[currentPoint].y);
                ++n_points;
            }

        }
        printf("\nnumber of points:%d\n", n_points);
        printf("\n");
    }
}

void calculateCentroids(int nCluster, int nPoints, point *points, cluster *clusters) {
    //for each cluster
    for (int currentCluster = 0; currentCluster < nCluster; ++currentCluster) {
        int n_points = 0;
        point p;
        p.x = 0;
        p.y = 0;
        //for each point
        for (int currentPoint = 0; currentPoint < nPoints; ++currentPoint) {
            //if the point belongs to the cluster
            if (points[currentPoint].clusterNumber == currentCluster) {
                p = sum(p, points[currentPoint]);
                ++n_points;
            }
        }
        //calculate new new centroid
        if (n_points != 0) {
            clusters[currentCluster].centroid = divide(p, n_points);
        } else {
            clusters[currentCluster].centroid = p;
        }
    }
}


int main() {
    //Input
    int nCluster, nPoints, nIterations;
    scanf("%d\n%d\n%d\n", &nCluster, &nPoints, &nIterations);
    cluster *clusters = (cluster *) malloc(nCluster * sizeof(cluster));
    point *points = (point *) malloc(nPoints * sizeof(point));
    for (int i = 0; i < nPoints; ++i) {
        scanf("%lf %lf\n", &(points[i].x), &(points[i].y));
        points[i].clusterNumber = i % nCluster;
    }
    //End of input
    double start = omp_get_wtime();
    //region to be Parallelized
    calculateCentroids(nCluster, nPoints, points, clusters);
    //for each iteration
    for (int i = 0; i < nIterations; ++i) {
        //for each cluster
        for (int currentCluster = 0; nCluster < currentCluster; ++currentCluster) {
            //for each point
            for (int currentPoint = 0; currentPoint < nPoints; ++currentPoint) {
                point p = points[currentPoint];
                //if the point belongs to the currentCluster
                if (p.clusterNumber == currentCluster) {
                    continue;
                }
                //else
                //if the current point is farther from his cluster in comparison to the cluster being analyzed
                if (distance(p, clusters[p.clusterNumber].centroid) > distance(p, clusters[currentCluster].centroid)) {
                    points[currentPoint].clusterNumber = currentCluster;
                }
            }
        }
        //recalculate each centroid after the end of iteration
        calculateCentroids(nCluster, nPoints, points, clusters);
    }


    point global_centroid;
    global_centroid.x = 0;
    global_centroid.y = 0;

    //use each cluster to obtain global centroid
    for (int currentCluster = 0; currentCluster < nCluster; ++currentCluster) {
        global_centroid = sum(global_centroid, clusters[currentCluster].centroid);
    }
    global_centroid = divide(global_centroid, nCluster);
    double end = omp_get_wtime();

    printClusters(nCluster, nPoints, points, clusters);

    printf("global centroid:(%lf, %lf)\n", global_centroid.x, global_centroid.y);

    printf("elapsed time %lfs\n", end - start);
    free(points);
    free(clusters);
    return 0;
}