CC=gcc
CFLAGS= -lm -fopenmp
all: kmeans

%.o: %.c
	$(CC) -c -o $@ $< -lm -fopenmp

kmeans: kmeans.o kmeans.o
	$(CC) -o kmeans kmeans.o $(CFLAGS)

clean:
	rm kmeans kmeans.o