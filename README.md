# Omp-Task-lab7
## Kmeans
k-means clustering is a method of vector quantization, originally from signal processing, that is popular for cluster analysis in data mining. k-means clustering aims to partition n observations into k clusters in which each observation belongs to the cluster with the nearest mean.
![clusters](img/SlpL1.png)

### Description
In this problem we have to group each point accordingly each cluster centroid proximity.

### Input
- one line containing the number of clusters (**C**)
- one line containing the number of 2D points (**P**)
- one line containing the number of iterations (**I**)
- **P** lines containing each one point 

### Output
- **C** clusters and it's points and centroids
- The global cluster (average of all centroids)
- The elapsed time between the star and the end of computation

## Hints
- each iterations updates the centroids, therefore each iteration depends on the previous, in order to synchronize you
  have to establish a sync point
- calculating the new centroid of each cluster in each iteration is an independent process
- it's advised to use test 13 (**t13**) in order to measure the speedup
- perform a diff between every sequential output and every parallel output, only the elapsed time may differ 

### Compiling and running
```shell
make
./kmeans < /input/tXX
```
 ### remarks
 Speedup obtained using *i5-8250U* = 4,09 
 
 #### TODO:
 
 1. Fork the repository to your own account.
 2. Clone your forked repository on your computer.
 3. Study the code using your favorite editor.
 4. Parallelize the code using OpenMP.
 5. Compare performance between serial and parallel version. Determine the speedup using the test **t13**


Anything missing? Ideas for improvements? Make a pull request.
